import React from 'react';
import ResetPasswordForm from '../components/ResetPasswordForm';

const ResetPasswordView = () => {
  return (
    <div>
      <ResetPasswordForm />
    </div>
  );
};

export default ResetPasswordView;
