import { createStore, combineReducers } from 'redux';
import authReducer from './reducers/auth';
import receiptReducer from './reducers/receipt';

//sometimes called appReducers, or just reducers
//pass an object into combinedReducers
const rootReducers = combineReducers({
  auth: authReducer,
  receipt: receiptReducer,
});

export default createStore(
  rootReducers,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
