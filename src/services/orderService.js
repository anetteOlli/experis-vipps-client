import axios from 'axios';
import { BASE_URL } from '../constants/constants';

class orderService {
  //get all orders
  getAllOrders = async () => {
    const token = localStorage.getItem('accessToken');
    return axios
      .get(BASE_URL + '/api/v1/order', {
        headers: {
          authorization: 'Bearer ' + token,
          'Content-Type': 'application/json',
        },
      })
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  };

  addOrder = async (status, item, idemKey, name, email, address) => {
    const token = localStorage.getItem('accessToken');
    return axios({
      method: 'post',
      url: BASE_URL + '/api/v1/order/',
      data: {
        status: status,
        itemsIDs: item,
        idemKey: idemKey,
        name: name,
        email: email,
        address: address,
      },
      headers: {
        authorization: 'Bearer ' + token,
        'Content-Type': 'application/json',
      },
    });
  };

  updateOrder = async (status, idemKey) => {
    const token = localStorage.getItem('accessToken');
    return axios({
      method: 'put',
      url: BASE_URL + '/api/v1/order/',
      data: {
        status: status,
        idemKey: idemKey,
      },
      headers: {
        authorization: 'Bearer ' + token,
        'Content-Type': 'application/json',
      },
    });
  };

  deleteOrder = async (id) => {
    return axios.delete(BASE_URL + '/api/v1/order/' + id);
  };
}

export default orderService;
