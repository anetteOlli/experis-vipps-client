import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';

import userService from '../services/userService';
import {
  BASE_URL,
  AUTHORIZE_URL,
  MINLEN_PHONE,
  MINLEN_ADDRESS,
  MINLEN_NAME,
  MINLEN_EMAIL,
} from '../constants/constants';
import StatusCodes from 'http-status-codes';
import PasswordInput from '../components/PasswordInput';

//design
import TextField from '@material-ui/core/TextField';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import '../styles/landingView.css';
import CircularProgress from '@material-ui/core/CircularProgress';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    width: '45ch',
    margin: theme.spacing(1),
  },
  googleButton: {
    width: '45ch',
  },
}));

function RegisterView() {
  const history = useHistory();
  const userServ = new userService();
  const classes = useStyles();

  function Form() {
    const [email, setEmail] = useState('');
    const [gname, setGName] = useState('');
    const [fname, setFName] = useState('');
    const [address, setAddress] = useState('');
    const [phone, setPhone] = useState('');
    const [password, setPassword] = useState('');
    const [passwordIsOkay, setPasswordIsOkay] = useState(false);

    const [readyForSubmit, setReadyForSubmit] = useState(false);

    const [backendIsLoading, setBackendIsLoading] = useState(false);

    function onSubmit(event) {
      event.preventDefault();
      setBackendIsLoading(true);
      userServ
        .addUser(email, gname, fname, address, phone, password)
        .then((response) => {
          if (response.status === StatusCodes.CREATED) {
            // User was successfully created (Http-Re: 201)
            //Redirect to verification page
            sessionStorage.setItem('email', email);
            history.push('/verify');
          } else {
            alert('Could not register your account, please try again');
          }
        });
    }

    const possibleShowSubmitButton = () => {
      //If either input is too short then the submit button is disabled.
      setReadyForSubmit(
        !(
          email.length < MINLEN_EMAIL ||
          gname.length < MINLEN_NAME ||
          fname.length < MINLEN_NAME ||
          passwordIsOkay ||
          backendIsLoading
        )
      );
    };

    return (
      <div>
        {backendIsLoading && <CircularProgress />}
        {!backendIsLoading && (
          <form onSubmit={onSubmit}>
            <label className={classes.textField}>
              Fields marked with * are required.
            </label>{' '}
            <br />
            <TextField
              required
              label='Email'
              type='email'
              className={classes.textField}
              onChange={(e) => {
                setEmail(e.target.value);
                possibleShowSubmitButton();
              }}
            />
            <br />
            <PasswordInput
              password={(e) => setPassword(e)}
              passwordIsOkay={(e) => setPasswordIsOkay(e)}
            ></PasswordInput>
            <br />
            <TextField
              required
              label='Given name'
              type='text'
              onChange={(e) => {
                setGName(e.target.value);
                possibleShowSubmitButton();
              }}
              minLength={MINLEN_NAME}
              className={classes.textField}
            />
            <br />
            <TextField
              required
              label='Family name'
              type='text'
              onChange={(e) => {
                setFName(e.target.value);
                possibleShowSubmitButton();
              }}
              minLength={MINLEN_NAME}
              className={classes.textField}
            />
            <br />
            <TextField
              label='Address'
              type='text'
              onChange={(e) => {
                setAddress(e.target.value);
                possibleShowSubmitButton();
              }}
              minLength={MINLEN_ADDRESS}
              className={classes.textField}
            />
            <br />
            <FormControl className={classes.textField}>
              <InputLabel htmlFor='telephone'>Telephone</InputLabel>
              <Input
                id='telephone'
                type='tel'
                onChange={(e) => {
                  setPhone(e.target.value);
                  possibleShowSubmitButton();
                }}
                minLength={MINLEN_PHONE}
              />
            </FormControl>
            <br />
            <div id='buttonContainer'>
              <Button
                variant='contained'
                type='submit'
                disabled={!readyForSubmit}
              >
                Sign up
              </Button>
            </div>
          </form>
        )}
      </div>
    );
  }

  return (
    <div className={classes.root}>
      <Container maxWidth='sm'>
        <div id='headerContent'>
          <h1>Register a new account</h1>
        </div>
        <div>
          <Button
            className={classes.googleButton}
            variant='contained'
            size='large'
            color='primary'
            href={BASE_URL + AUTHORIZE_URL}
          >
            Sign up with Google
          </Button>
          <br />
          <br />
          <br />
          <Form />
        </div>
      </Container>
    </div>
  );
}

export default RegisterView;
