import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { setAuth } from '../store/actions/auth';
import { ACCESS_TOKEN } from '../constants/constants';
import Button from '@material-ui/core/Button';
import { useHistory } from 'react-router-dom';

const LogoutButton = () => {
  const auth = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const history = new useHistory();

  const logout = () => {
    localStorage.removeItem(ACCESS_TOKEN);
    //localStorage.clear();
    dispatch(
      setAuth({
        isLoggedIn: false,
        accessToken: '',
      })
    );
    history.push('/welcome');
  };

  return (
    <div>
      {auth.isLoggedIn && (
        <Button variant='contained' color='primary' onClick={logout}>
          Log out
        </Button>
      )}
    </div>
  );
};

export default LogoutButton;
