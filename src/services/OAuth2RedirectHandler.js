import React, { useEffect, useState } from 'react';
import { Redirect } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { setAuth } from '../store/actions/auth';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

const Alert = (props) => {
  return <MuiAlert elevation={6} variant='filled' {...props} />;
};

const OAuth2RedirectHandler = (props) => {
  const auth = useSelector((state) => state.auth);
  const [open, setOpen] = useState(false);
  const [failure, setFailure] = useState(false);
  const dispatch = useDispatch();
  const token = new URLSearchParams(props.location.search).get('token');
  const errorMessage = new URLSearchParams(props.location.search).get('error');

  useEffect(() => {
    if (errorMessage === null) {
      localStorage.setItem('accessToken', token);
      dispatch(
        setAuth({
          isLoggedIn: true,
          accessToken: token,
        })
      );
    } else {
      setOpen(true);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dispatch, token]);

  const handleSnackBarClose = () => {
    setFailure(true);
  };

  return (
    <div>
      {auth.isLoggedIn && (
        <Redirect
          to={{
            pathname: '/welcome',
            state: {
              from: props.location,
            },
          }}
        />
      )}
      {errorMessage !== null && (
        <Snackbar
          anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
          open={open}
        >
          <Alert severity='error' onClose={handleSnackBarClose}>
            Could not log in with google
          </Alert>
        </Snackbar>
      )}
      {failure && (
        <Redirect
          to={{
            pathname: '/login',
            state: {
              from: props.location,
            },
          }}
        />
      )}
    </div>
  );
};

export default OAuth2RedirectHandler;
