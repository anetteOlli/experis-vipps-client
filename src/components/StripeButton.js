import React, { useState } from 'react';
import StripeCheckout from 'react-stripe-checkout';
import axios from 'axios';
import { v4 as uuidv4 } from 'uuid';
import { BASE_URL, ITEM_STATUS } from '../constants/constants';
import orderService from '../services/orderService';
import { STRIPE_PK } from '../constants/constants';
import { useDispatch } from 'react-redux';
import { setReceipt } from '../store/actions/receipt';
import { Redirect } from 'react-router-dom';

// Make sure to call `loadStripe` outside of a component’s render to avoid
// recreating the `Stripe` object on every render.

export default function StripeButton(props) {
  let totalPrice = 0;
  const orderServ = new orderService();
  const dispatch = useDispatch();
  const [finishedProcessing, setFinishedProcessing] = useState(false);

  //Sum the price of all listed items for total price
  for (let i = 0; i < props.items.length; i++) {
    totalPrice += props.items[i].price;
  }
  totalPrice = totalPrice * 100; // Initial price is in 1/100 of a Norwegian Krone.

  //Get the names from the items for the description
  const getItemNames = () => {
    let itemNames = '';
    for (let i = 0; i < props.items.length; i++) {
      itemNames += props.items[i].name + ', ';
    }
    return itemNames;
  };

  const cart = {
    name: 'Cart',
    price: totalPrice,
    quantity: 1,
    idemKey: 'tempKey',
  };
  cart.idemKey = uuidv4();
  async function purchaseToken(theToken) {
    orderServ.addOrder(ITEM_STATUS.IN_PROGRESS, startReciept(), cart.idemKey, props.name, props.email, props.address);
    const result = await axios.post(BASE_URL + '/create-session', {
      theToken,
      cart,
    });
    const status = result.data;
    if (status === 'Success') {
      alert('Purchase completed');
      orderServ.updateOrder(ITEM_STATUS.COMPLETED, cart.idemKey).then((res) => {
        dispatch(setReceipt(res.data));

        setFinishedProcessing(true);
      });
    } else {
      alert(
        'Something went wrong.\nPlease press "Pay With Card" again, you will not be charged double.'
      );
      orderServ.updateOrder(ITEM_STATUS.FAILED, cart.idemKey).then((res) => {
        dispatch(setReceipt(res.data));
        //setFinishedProcessing(true);
      });
    }
  }

  const startReciept = () => {
    let itemIds = [];
    for (let i = 0; i < props.items.length; i++) {
      itemIds.push(props.items[i].itemId);
    }
    return itemIds;
  };
  return (
    <React.Fragment>
      {finishedProcessing && (
        <Redirect
          to={{
            pathname: '/receipt',
            state: {
              from: props.location,
            },
          }}
        />
      )}

      <StripeCheckout
        stripeKey={STRIPE_PK}
        token={purchaseToken}
        name={cart.name}
        amount={cart.price}
        currency='NOK'
        description={getItemNames()}
        email={props.email}
      ></StripeCheckout>
    </React.Fragment>
  );
}
