//types:
export const ACTION_SET_RECEIPT = 'ACTION_SET_RECEIPT';

//actions:
export const setReceipt = (receipt) => ({
  type: ACTION_SET_RECEIPT,
  payload: receipt,
});
