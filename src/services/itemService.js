import axios from 'axios';
import { BASE_URL } from '../constants/constants';

class itemService {
  //get 0-4 random items
  getRandomItems = async () => {
    return axios.get(BASE_URL + '/api/v1/item/random');
  };
  getSpecificItem = async (uri) => {
    return axios.get(`${BASE_URL}${uri}`).then((res) => {
      return res.data;
    });
  };
}

export default itemService;
