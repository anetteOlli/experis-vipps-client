import { List, ListItem, ListItemText } from '@material-ui/core';
import React from 'react';
import Alert from '@material-ui/lab/Alert';
import '../styles/itemDisplay.css';

//Renders differently if the cart only contains one item, this is to make the styling correct.
function ItemDisplay(props) {
  const itemList = props.items.map((item, index) => {
    if (props.items.length === 1) {
      return (
        <div key={index}>
          <ListItem>
            <ListItemText
              primary={item.name}
              key={item.id}
              secondary={item.price + ' NOK'}
            />
          </ListItem>
        </div>
      );
    }
    //Render a small line to separate items
    return (
      <div key={index}>
        {props.items[0].name === item.name ? (
          <div></div>
        ) : (
          <div id='line'></div>
        )}
        <ListItem>
          <ListItemText primary={item.name} secondary={item.price + ' NOK'} />
        </ListItem>
      </div>
    );
  });

  if (props.items.length === 0) {
    return (
      <div>
        <Alert severity='warning'>Your cart is empty!</Alert>
      </div>
    );
  }

  return (
    <div>
      <List>{itemList}</List>
    </div>
  );
}

export default ItemDisplay;
