import React from 'react';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import '../styles/landingView.css';

function LandingView() {
  const auth = useSelector((state) => state.auth);
  return (
    <div>
      <Container maxWidth='md'>
        <div id='headerContent'>
          <h1>Welcome</h1>
        </div>
        <div id='buttonContainer'>
          {auth.isLoggedIn && (
            <Button
              variant='outlined'
              id='button'
              size='large' 
              color='primary' 
              component={Link} 
              to='/profile'>
                Profile
            </Button>
          )}
          {!auth.isLoggedIn && (
          <Button 
            variant='outlined' 
            id='button' 
            size='large' 
            color='primary' 
            component={Link} 
            to='/login'>
              Log in
          </Button>
          )}
          {!auth.isLoggedIn && (
          <Button 
            variant='outlined' 
            id='button' 
            size='large' 
            color='primary' 
            to='/register' 
            component={Link}>
              Register
          </Button>
          )}
          <Button
            component={Link}
            to='/purchase'
            variant='outlined'
            id='button' 
            size='large'
            color='primary'
          >
              Purchase
          </Button>
          {auth.isLoggedIn && (
            <Button
              component={Link}
              to='/receipt'
              variant='outlined'
              id='button' 
              size='large'
              color='primary'
            >
              receipts
            </Button>
          )}
        </div>
      </Container>
    </div>
  );
}

export default LandingView;
