import React, { useState, useEffect } from 'react';
import orderService from '../services/orderService';
import List from '@material-ui/core/List';
import ReceiptListItem from '../components/ReceiptListItem';
import StatusCodes from "http-status-codes";
import Alert from '@material-ui/lab/Alert';

const ReceiptList = () => {
  const [receipts, setReceipts] = useState([]);

  useEffect(() => {
    const orderservice = new orderService();
    orderservice.getAllOrders().then((res) => {
      if (res.status === StatusCodes.OK) {
        setReceipts([...res.data]);
      } else {
        console.error("getAllOrders() failed (Error "+res.status+')');
      }
    });
  }, []);

  if (receipts.length === 0) {
    return (
      <div>
        <Alert severity='warning'>You have no receipts!</Alert>
      </div>
    );
  }

  return (
    <React.Fragment>
      <List>
        {receipts.map((value, index) => {
          return <ReceiptListItem data={value} key={index}></ReceiptListItem>;
        })}
      </List>
    </React.Fragment>
  );
};

export default ReceiptList;
