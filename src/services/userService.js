import axios from 'axios';
import { BASE_URL } from '../constants/constants';

class userService {
  //get me
  getCurrentUser = async () => {
    const token = localStorage.getItem('accessToken');
    return axios
      .get(BASE_URL + '/api/v1/customer', {
        headers: {
          authorization: 'Bearer ' + token,
          'Content-Type': 'application/json',
        },
      })
      .then((response) => {
        return response;
      })
      .catch((err) => {
        return err.response;
      });
  };

  //get all users
  getAllUsers = async () => {
    return axios.get(BASE_URL + '/api/v1/customer/getall');
  };

  addUser = async (email, givenName, familyName, address, phone, password) => {
    return axios
      .post(`${BASE_URL}/auth/signup`, {
        email: email,
        givenName: givenName,
        familyName: familyName,
        address: address,
        phone: phone,
        password: password,
      })
      .then((response) => {
        return response;
      })
      .catch((err) => {
        //console.log(err);
        return err.response;
      });
  };

  updateUser = async (customerId, givenName, familyName, address, phone) => {
    const token = localStorage.getItem('accessToken');
    return axios
      .put(
        `${BASE_URL}/api/v1/customer`,
        {
          customerId: customerId,
          givenName: givenName,
          familyName: familyName,
          address: address,
          phone: phone,
        },
        {
          headers: {
            authorization: 'Bearer ' + token,
            'Content-Type': 'application/json',
          },
        }
      )
      .then((response) => {
        return response;
      })
      .catch((err) => {
        //
        return err.response;
      });
  };

  verifyUserLogin = async (email, password, verificationKey) => {
    return axios.put(`${BASE_URL}/auth/verify-login`, {
      email,
      password,
      verificationKey,
    }).then((response) => {
      return response;
    })
    .catch((err) => {
      return err.response;
    });
  };

  verifyUserRegister = async (email, verificationKey) => {
    const password = "empty";
    return axios.put(`${BASE_URL}/auth/verify-register`, {
      email,
      password,
      verificationKey,
    }).then((response) => {
      return response;
    })
    .catch((err) => {
      return err.response;
    });
  };

  loginUser = async (email, password) => {
    return axios
      .post(`${BASE_URL}/auth/login`, {
        email,
        password,
      })
      .then((response) => {
        return response;
      })
      .catch((err) => {
        return err.response;
      });
  };

  resetVerificationCode = async (email) => {
    return axios
      .put(`${BASE_URL}/auth/resetcode`, {
        username: email,
      })
      .then((response) => {
        return response;
      })
      .catch((err) => {
        return err.response;
      });
  };

  resetPassword = async (email, verificationCode, password) => {
    return axios
      .put(`${BASE_URL}/api/v1/customer/resetpassword`, {
        username: email,
        verificationKey: verificationCode,
        password: password,
      })
      .then((response) => {
        return response;
      })
      .catch((err) => {
        return err.response;
      });
  };
}
export default userService;
