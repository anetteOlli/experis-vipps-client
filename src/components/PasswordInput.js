import React, { useState } from 'react';

import { MINLEN_PASSWORD } from '../constants/constants';

//design
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import FormControl from '@material-ui/core/FormControl';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    width: '45ch',
    margin: theme.spacing(1),
  },
  googleButton: {
    width: '45ch',
  },
}));

const PasswordInput = (props) => {
  const classes = useStyles();
  const [password1, setPassword1] = useState('');
  const [showPassword1Error, setShowPassword1Error] = useState(false);
  const [showPassword2Error, setShowPassword2Error] = useState(false);
  const [password2, setPassword2] = useState('');
  const [showPassword, setShowPassword] = useState(false);
  const [password2Touched, setPassword2Touched] = useState(false);
  const [password1Touched, setPassword1Touched] = useState(false);

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const handlePassword1Change = (password1Input) => {
    setPassword1Touched(true);
    setPassword1(password1Input);

    if (password2Touched) {
      setShowPassword2Error(password1Input !== password2);
    }

    setShowPassword1Error(password1.length < MINLEN_PASSWORD);
    possibleToContinue();
  };

  const possibleToContinue = () => {
    props.passwordIsOkay(
      password1 === password2 && password1.length < MINLEN_PASSWORD
    );
    props.password(password1);
  };

  const handlePassword2Change = (password2Input) => {
    setPassword2Touched(true);
    setPassword2(password2Input);

    setShowPassword2Error(password2Input !== password1);
    possibleToContinue();
  };

  return (
    <React.Fragment>
      <FormControl className={classes.textField}>
        <InputLabel htmlFor='standard-adornment-password1'>
          {!(password1Touched && showPassword1Error) && 'Password *'}
          {password1Touched &&
            showPassword1Error &&
            'Password must be at least ' +
              MINLEN_PASSWORD +
              ' characters long.'}
        </InputLabel>
        <Input
          required
          id='standard-adornment-password1'
          type={showPassword ? 'text' : 'password'}
          minLength={MINLEN_PASSWORD}
          onChange={(e) => {
            handlePassword1Change(e.target.value);
          }}
          endAdornment={
            <InputAdornment position='end'>
              <IconButton
                aria-label='Toggle password visibility'
                onClick={handleClickShowPassword}
                onMouseDown={handleMouseDownPassword}
              >
                {showPassword ? <Visibility /> : <VisibilityOff />}
              </IconButton>
            </InputAdornment>
          }
        />
      </FormControl>
      <br />
      <FormControl className={classes.textField}>
        <InputLabel htmlFor='standard-adornment-password2'>
          {!password2Touched && 'Repeat password *'}
          {password2Touched &&
            !password1Touched &&
            'You must enter a password in the previous field first! *'}
          {password2Touched &&
            password1Touched &&
            showPassword2Error &&
            "Passwords don't match! *"}
          {password2Touched &&
            password1Touched &&
            !showPassword2Error &&
            'Password repeated. *'}
        </InputLabel>

        <Input
          required
          id='standard-adornment-password2'
          type={showPassword ? 'text' : 'password'}
          minLength={MINLEN_PASSWORD}
          value={password2}
          error={showPassword2Error}
          onChange={(e) => {
            handlePassword2Change(e.target.value);
          }}
          endAdornment={
            <InputAdornment position='end'>
              <IconButton
                aria-label='Toggle password visibility'
                onClick={handleClickShowPassword}
                onMouseDown={handleMouseDownPassword}
              >
                {showPassword ? <Visibility /> : <VisibilityOff />}
              </IconButton>
            </InputAdornment>
          }
        />
      </FormControl>
    </React.Fragment>
  );
};

export default PasswordInput;
