import Container from '@material-ui/core/Container';
import React, { useEffect, useState } from 'react';
import StripeButton from '../components/StripeButton';
import itemService from '../services/itemService';
import userService from '../services/userService';
import ItemDisplay from '../components/ItemDisplay';
import CheckoutForm from '../components/CheckoutForm';
import '../styles/purchaseView.css';
import { Paper } from '@material-ui/core';

function PurchaseView() {
  const [items, setItems] = useState([]);
  const [theUser, setTheUser] = useState({});
  const [showButton, setShowButton] = useState(false);
  const [email, setEmail] = useState(null);
  const [name, setName] = useState(null);
  const [address, setAddress] = useState(null);

  useEffect(() => {
    const itemServ = new itemService();
    const userServ = new userService();
    itemServ.getRandomItems().then((res) => setItems(res.data));
    userServ.getCurrentUser().then((res) => setTheUser(res.data));
  }, []);

  const handleData = (formData) => {
    if (
      formData.email === '' ||
      formData.name === '' ||
      formData.adress === '' ||
      formData.email === null ||
      formData.name === null ||
      formData.adress === null
    ) {
      alert('Please complete the form before saving.');
      setShowButton(false);
    } else {
      setEmail(formData.email);
      setName(formData.name);
      setAddress(formData.adress);
      setShowButton(true);
    }
  };

  return (
    <Container maxWidth='md'>
      <h1>Your cart</h1>
      <div id='container'>
        <div id='itemView'>
          <Paper>
            <ItemDisplay items={items}></ItemDisplay>
          </Paper>
          <div id='buttonView'>
            {showButton ? (
              <StripeButton email={email} name={name} address={address} items={items}></StripeButton>
            ) : null}
          </div>
        </div>
        <div id='formView'>
          {items.length === 0 ? (
            <div></div>
          ) : (
            <CheckoutForm
              sendData={handleData}
              theUser={theUser}
            ></CheckoutForm>
          )}
        </div>
      </div>
    </Container>
  );
}

export default PurchaseView;
