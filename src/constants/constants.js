// Client access
export const BASE_URL = 'https://experis-case-vipps.herokuapp.com';
export const AUTHORIZE_URL =
  '/oauth2/authorize/google?redirect_uri=https://experis-vipps-client.herokuapp.com/oauth2/redirect';
export const ACCESS_TOKEN = 'accessToken';
export const STRIPE_PK =
  'pk_test_51HbN7VFyNmEWskZujPL1PAkFmN3ahSJkNF8f0iEG5vnRJkIaP04pLcqyLDRv5Jgc7sFZebv0byWoWWq2ysqDpIvZ00jbMxy4GY';

// Minimum input lengths
export const MINLEN_NAME = 2;
export const MINLEN_EMAIL = 4;
export const MINLEN_PASSWORD = 6;
export const MINLEN_ADDRESS = 6;
export const MINLEN_PHONE = 8; // Can technically be lower if very old

// Enum used by items, to reduce risk of undefined states.
export const ITEM_STATUS = {
  IN_PROGRESS: 'IN_PROGRESS',
  COMPLETED: 'COMPLETED',
  FAILED: 'FAILED',
  INVALID_STATUS: 'INVALID_STATUS',
};
