# Group 5 - Case Vipps Client

A distributed software solution for accepting orders for a digital product, using a 3rd party payment processor.
This repo holds the client side of the solution. It expects the [server side](https://gitlab.com/nbdnguyen/case-vipps) running. See the other repo.

### Group members

- @anetteOlli - Anette Olli Siiri
- @Patrickpt - Patrick Thorkildsen
- @Eiriksaur - Eirik Eide Saur
- @SteinarP - Steinar Valheim Pedersen
- @NbdNguyen - Nghi Nguyen

## Project Overview:

- A web front-end
  - A landing page: login (anynomous user authenticates) or purchase (anynomous user proceeds to payment gateway)
  - A purchase page: 0-4 generated random items

## Tech - Dependencies

Node: minimum requrements: 12.8
https://nodejs.org/en/


## Installation instructions

### Run as developer:

```javascript
npm install
npm start
```


### Deploying:


```javascript
npm install
npm run build
npm install -g serve
serve -s build
```

## See also:

- ### [API Documentation](https://documenter.getpostman.com/view/13215254/TVYDffDS)
- ### [User Manual](https://gitlab.com/nbdnguyen/case-vipps/-/blob/master/documentation/USER-MANUAL.md)
- ### [Server](https://gitlab.com/nbdnguyen/case-vipps) 
