import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import itemService from '../services/itemService';
import ItemTable from './ItemTable';

//design imports
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import {ITEM_STATUS} from "../constants/constants";

const ReceiptDetails = () => {
  const receipt = useSelector((state) => state.receipt);
  const [items, setItems] = useState([]);

  useEffect(() => {
    const itemservice = new itemService();
    async function fetchItems() {
      let newList = [];
      for (let i = 0; i < receipt.items.length; i++) {
        newList.push(await itemservice.getSpecificItem(receipt.items[i]));
      }
      setItems(newList);
    }
    fetchItems();
  }, [receipt.items]);

  return (
    <React.Fragment>
      {receipt.receiptId !== 0 && (
        <Card>
          <CardContent>
            <Typography variant='h5' component='h2'>
              Receipt ID: {receipt.receiptId}
            </Typography>
            <br />
            <Typography>Purchased at: {receipt.time}</Typography>
            <br />

            {receipt.status !== ITEM_STATUS.COMPLETED && (
              <Typography color='error'>Status: {receipt.status}</Typography>
            )}
            {receipt.status === ITEM_STATUS.COMPLETED && (
              <Typography>Status: {receipt.status}</Typography>
            )}
            <br />
            <ItemTable items={items}></ItemTable>
            <Typography>Total Price: {receipt.totalPrice}</Typography>
          </CardContent>
        </Card>
      )}
    </React.Fragment>
  );
};

export default ReceiptDetails;
