
Usage first requires installation. See installation instructions at [README](https://gitlab.com/nbdnguyen/case-vipps/-/blob/master/README.md) file.

For regular usage, see the [user manual](https://gitlab.com/nbdnguyen/case-vipps/-/blob/master/documentation/USER-MANUAL.md) on the server side's documentation.