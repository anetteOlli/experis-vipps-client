import React from 'react';
import ReceiptList from '../components/ReceiptList';
import { useSelector } from 'react-redux';

//design imports:
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import ReceiptDetails from '../components/ReceiptDetails';

const ReceiptView = () => {
  const auth = useSelector((state) => state.auth);

  return (
    <React.Fragment>
      <Container maxWidth='sm'>
        <h1>Your Receipts</h1>
        <br />
        {auth.isLoggedIn && (
          <Box>
            <ReceiptList></ReceiptList>
          </Box>
        )}

        <Box>
          <ReceiptDetails></ReceiptDetails>
        </Box>
      </Container>
    </React.Fragment>
  );
};

export default ReceiptView;
