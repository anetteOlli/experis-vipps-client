import React from 'react';
import ReactDOM from 'react-dom';
import ItemTable from './ItemTable';
import { Provider } from 'react-redux';
import store from '../store/store';
import { BrowserRouter } from 'react-router-dom';

let container;

beforeEach(() => {
  container = document.createElement('div');
  document.body.appendChild(container);
});

afterEach(() => {
  document.body.removeChild(container);
  container = null;
});

it('renders without crashing', () => {
  const props = [
    { itemId: 1, name: 'dog', price: 53.4 },
    { itemId: 2, name: 'cat', price: 50 },
  ];
  ReactDOM.render(
    <Provider store={store}>
      <BrowserRouter>
        <ItemTable items={props}></ItemTable>
      </BrowserRouter>
    </Provider>,
    container
  );
});
