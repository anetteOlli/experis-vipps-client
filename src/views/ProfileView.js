import React, { useEffect, useState } from 'react';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import userService from '../services/userService';
import '../styles/profileView.css';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';
import StatusCodes from 'http-status-codes';
import TextField from '@material-ui/core/TextField';
import {
  MINLEN_ADDRESS,
  MINLEN_NAME,
  MINLEN_PHONE,
} from '../constants/constants';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    width: '45ch',
    margin: theme.spacing(1),
  },
  googleButton: {
    width: '45ch',
  },
}));

function ProfileView() {
  //const auth = useSelector((state) => state.auth);
  const [theUser, setTheUser] = useState({});
  const classes = useStyles();
  const history = new useHistory();

  useEffect(() => {
    const userServ = new userService();
    userServ.getCurrentUser().then((res) => {
      //Check if user is logged in, redirect if not.
      if (res.status === StatusCodes.OK) {
        setTheUser(res.data);
      } else {
        alert('You need to be logged in to access your profile');
        history.push('/');
      }
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  function Form() {
    const [gname, setGName] = useState(theUser.givenName + '');
    const [fname, setFName] = useState(theUser.familyName + '');
    const [address, setAddress] = useState(theUser.address + '');
    const [phone, setPhone] = useState(theUser.phone + '');
    const [readyForSubmit, setReadyForSubmit] = useState(false);

    function onSubmit(event) {
      //If submitted, the profile will be updated.
      event.preventDefault();
      const userServ = new userService();
      userServ
        .updateUser(theUser.customerId, gname, fname, address, phone)
        .then((response) => {
          if (response.status) {
            if (response.status === StatusCodes.OK) {
              window.location.reload();
            } else if (response.status === StatusCodes.BAD_REQUEST) {
              alert(
                'Either input length is too short or something else went wrong, ' +
                  'please try again.'
              );
            }
          } else {
            alert(
              'Could not update information, please try again: ' +
                response.data.message
            );
          }
        });
    }

    const possibleShowSubmitButton = () => {
      //If either input is too short then the submit button is disabled.
      setReadyForSubmit(
        !(
          gname.length < MINLEN_NAME ||
          fname.length < MINLEN_NAME ||
          address.length < MINLEN_ADDRESS ||
          phone.length < MINLEN_PHONE
        )
      );
    };

    return (
      <div>
        <TextField
          label={theUser.email}
          type='text'
          className={classes.textField}
          placeholder='Email cannot be changed. Please register a new account instead.'
          disabled
        />
        <form onSubmit={onSubmit}>
          <TextField
            label={theUser.givenName}
            type='text'
            className={classes.textField}
            placeholder='Input new given name'
            onChange={(e) => {
              setGName(e.target.value);
              possibleShowSubmitButton();
            }}
            minLength={MINLEN_NAME}
          />
          <br />
          <TextField
            label={theUser.familyName}
            type='text'
            className={classes.textField}
            placeholder='Input new family name'
            onChange={(e) => {
              setFName(e.target.value);
              possibleShowSubmitButton();
            }}
            minLength={MINLEN_NAME}
          />
          <br />
          <TextField
            label={theUser.address}
            type='text'
            className={classes.textField}
            placeholder='Input new address'
            onChange={(e) => {
              setAddress(e.target.value);
              possibleShowSubmitButton();
            }}
            minLength={MINLEN_ADDRESS}
          />
          <br />
          <TextField
            label={theUser.phone}
            type='tel'
            className={classes.textField}
            placeholder='Input new phone number'
            onChange={(e) => {
              setPhone(e.target.value);
              possibleShowSubmitButton();
            }}
            minLength={MINLEN_PHONE}
          />
          <br />
          <div id='buttonContainer'>
            <Button
              variant='contained'
              type='submit'
              disabled={!readyForSubmit}
            >
              Update
            </Button>
          </div>
        </form>
      </div>
    );
  }

  return (
    <div>
      <Container maxWidth='sm'>
        <div id='headerContent'>
          <h1>Profile</h1>
        </div>
        <Form />
      </Container>
    </div>
  );
}

export default ProfileView;
