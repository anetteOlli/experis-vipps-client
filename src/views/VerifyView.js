import { Button, TextField } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import React, { useEffect, useState } from 'react';
import userService from '../services/userService';
import Container from '@material-ui/core/Container';
import '../styles/verifyView.css';
import StatusCodes from 'http-status-codes';

function VerifyView() {
  const [typedCode, setTypedCode] = useState(null);
  const userServ = new userService();
  const history = useHistory();

  useEffect(() => {}, []);

  const checkRes = (resStatus) => {
    if (resStatus) {
      if (resStatus.status === StatusCodes.OK) {
        alert(
          'Your account has been verified. You will be redirected to the login page.'
        );
        history.push('/login');
        return;
      }
    } else {
      alert('Error: could not contact server.')
    }

    return;
  };

  const handleSubmit = () => {
    userServ
      .verifyUserRegister(sessionStorage.getItem('email'), typedCode)
      .then((res) => checkRes(res));
  };

  const sendNewVerificationCode = async () => {
    const res = await userServ.resetVerificationCode(
      sessionStorage.getItem('email')
    );
    if (res.status === StatusCodes.OK) {
      alert('The new verificationcode has been sent to your email');
    } else {
      alert('There was an internal error, try again');
    }
  };

  return (
    <div>
      <Container maxWidth='sm'>
        <form onSubmit={handleSubmit}>
          <div id='container'>
            <br />
            <TextField
              required
              label='5 digit code'
              onChange={(e) => {
                setTypedCode(e.target.value);
              }}
            ></TextField>
            <Button variant='contained' onClick={handleSubmit}>
              Verify
            </Button>
            <Button variant='contained' onClick={sendNewVerificationCode}>
              Send new
            </Button>
          </div>
        </form>
      </Container>
    </div>
  );
}

export default VerifyView;
