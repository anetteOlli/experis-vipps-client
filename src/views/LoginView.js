import React from 'react';
import { useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { BASE_URL, AUTHORIZE_URL } from '../constants/constants';
import LoginForm from '../components/LoginForm';
import Container from '@material-ui/core/Container';
import { Button } from '@material-ui/core';
import { Link } from 'react-router-dom';
import '../styles/loginView.css';

const LoginView = (props) => {
  const auth = useSelector((state) => state.auth);
  const link = BASE_URL + AUTHORIZE_URL;

  function toLink() {
    window.location.href=link;
  }

  return (
    <div>
      <Container maxWidth='sm'>
        {auth.isLoggedIn && (
          <Redirect
            to={{
              pathname: '/welcome',
              state: {
                from: props.location,
              },
            }}
          />
        )}
        <br />
          <Button
              variant='contained'
              color='primary'
              size='large'
              disableElevation
              onClick={toLink}
              id='button'
            >
              Login with Google
          </Button>
          <Button
              variant='contained'
              color='primary'
              size='large'
              disableElevation
              to={'/register'}
              component={Link}
              id='button'
            >
              No account? Register here.
          </Button>
        <br />
        <br />
        <LoginForm />
      </Container>
    </div>
  );
};

export default LoginView;
