import React from 'react';
import logo from './logo.svg';
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  BrowserRouter,
  Switch,
} from 'react-router-dom';
import LandingView from './views/LandingView';
import LoginView from './views/LoginView';
import Header from './components/Header';
import './App.css';
import PurchaseView from './views/PurchaseView';
import OAuth2RedirectHandler from './services/OAuth2RedirectHandler';
import RegisterView from './views/RegisterView';
import VerifyView from './views/VerifyView';
import ResetPasswordView from './views/ResetPasswordView';
import ProfileView from './views/ProfileView';
import ReceiptView from './views/ReceiptView';

function App() {
  return (
    <BrowserRouter>
      <Router>
        <Header></Header>
        <Switch>
          <Route exact path='/'>
            <Redirect to='/welcome'></Redirect>
          </Route>
          <Route exact path='/welcome' component={LandingView}></Route>
          <Route exact path='/purchase' component={PurchaseView}></Route>
          <Route exact path='/receipt' component={ReceiptView}></Route>
          <Route exact path='/login' component={LoginView}></Route>
          <Route exact path='/profile' component={ProfileView}></Route>
          <Route
            path='/oauth2/redirect'
            component={OAuth2RedirectHandler}
          ></Route>
          <Route path='/register' component={RegisterView}></Route>
          <Route path='/verify' component={VerifyView}></Route>
          <Route path='/password-reset' component={ResetPasswordView}></Route>
          <Route>
            <Redirect to='/welcome'></Redirect>
          </Route>
        </Switch>
      </Router>
    </BrowserRouter>
  );
}

export default App;
