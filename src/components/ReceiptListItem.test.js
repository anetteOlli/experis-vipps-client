import React from 'react';
import ReactDOM from 'react-dom';
import ReceiptListItem from './ReceiptListItem';
import { Provider } from 'react-redux';
import store from '../store/store';
import { BrowserRouter } from 'react-router-dom';
import { act } from 'react-dom/test-utils';
import {ITEM_STATUS} from "../constants/constants";

let container;

beforeEach(() => {
  container = document.createElement('div');
  document.body.appendChild(container);
});

afterEach(() => {
  document.body.removeChild(container);
  container = null;
});

it('renders without crashing', () => {
  const props = {
    idemKey: '6feda8d2-5d2a-4edd-a902-0d725eb3fcce',
    items: ['/api/v1/item/3', '/api/v1/item/2'],
    receiptId: 2,
    status: ITEM_STATUS.COMPLETED, //Completed
    time: '2020-10-20 08:33:50',
    totalPrice: 4025.8,
  };
  act(() => {});
  ReactDOM.render(
    <Provider store={store}>
      <BrowserRouter>
        <ReceiptListItem data={props}></ReceiptListItem>
      </BrowserRouter>
    </Provider>,
    container
  );
});
