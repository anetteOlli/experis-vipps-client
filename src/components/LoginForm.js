import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Button, Paper } from '@material-ui/core';
import userService from '../services/userService';
import { useDispatch } from 'react-redux';
import { setAuth } from '../store/actions/auth';
import { useHistory } from 'react-router-dom';
import StatusCodes from 'http-status-codes';
import CircularProgress from '@material-ui/core/CircularProgress';

function LoginForm(props) {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [emailSent, setEmailSent] = useState(false);
  const [verificationCode, setVerificationCode] = useState('');
  const [loadingBackend, setLoadingBackend] = useState('');

  const userServe = new userService();
  const dispatch = new useDispatch();
  const history = new useHistory();

  const useStyles = makeStyles((theme) => ({
    form: {
      '& > *': {
        margin: '5px',
        width: '90%',
      },
    },

    lines: {
      '& > *': {
        margin: '5px',
        width: '85%',
      },
    },
    button: {
      '& > *': {
        margin: '5px',
      },
    },
  }));

  const classes = useStyles();

  const handleUsernameChange = (event) => {
    setUsername(event.target.value);
  };
  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };
  const handleVerificationChange = (event) => {
    setVerificationCode(event.target.value);
  };

  const login = async (event) => {
    setLoadingBackend(true);
    const response = await userServe.loginUser(username, password);
    if (response.status === StatusCodes.OK) {
      setEmailSent(true);
    } else if (response.status === StatusCodes.BAD_REQUEST) {
      alert('User is not verified, please verify your account');
      sessionStorage.setItem('email', username);
      history.push('/verify');
    } else {
      alert('Incorrect Email or Password');
    }
    setLoadingBackend(false);
  };

  const verify = async () => {
    setLoadingBackend(true);
    const response = await userServe.verifyUserLogin(
      username,
      password,
      verificationCode
    );
    setLoadingBackend(false);
    if (response.status === StatusCodes.OK) {
      const token = response.data.accessToken;
      localStorage.setItem('accessToken', token);
      dispatch(
        setAuth({
          isLoggedIn: true,
          accessToken: token,
        })
      );
      history.push('/welcome');
    } else if (response.status === StatusCodes.BAD_REQUEST) {
      alert('Verification code does not match');
    } else {
      alert('Something went wrong, please try again.');
    }
  };

  const handleForgotPassword = () => {
    history.push('/password-reset');
  };

  return (
    <div>
      {loadingBackend && <CircularProgress />}
      {!loadingBackend && (
        <div>
          {' '}
          {!emailSent ? (
            <form className={classes.form}>
              <Paper>
                <TextField
                  className={classes.lines}
                  id='username'
                  label='Email'
                  type='email'
                  required
                  onChange={handleUsernameChange}
                />
                <br />
                <TextField
                  className={classes.lines}
                  id='password'
                  label='Password'
                  type='password'
                  required
                  onChange={handlePasswordChange}
                />
                <div className={classes.button}>
                  <Button variant='outlined' color='primary' onClick={login}>
                    Log in
                  </Button>{' '}
                  <Button
                    variant='outlined'
                    color='primary'
                    onClick={handleForgotPassword}
                  >
                    Forgot Password
                  </Button>
                </div>
              </Paper>
            </form>
          ) : null}
          {emailSent ? (
            <form className={classes.form}>
              <Paper>
                <TextField
                  className={classes.lines}
                  id='verificationCode'
                  label='Verification Code'
                  required
                  onChange={handleVerificationChange}
                />
                <div className={classes.button}>
                  <Button variant='outlined' color='primary' onClick={verify}>
                    Verify
                  </Button>
                </div>
              </Paper>
            </form>
          ) : null}
        </div>
      )}
    </div>
  );
}

export default LoginForm;
