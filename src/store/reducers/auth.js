import { ACTION_SET_USER } from '../actions/auth';
import { ACCESS_TOKEN } from '../../constants/constants';

//Remember that state must default to something
const authReducer = (
  state = {
    isLoggedIn: localStorage.getItem(ACCESS_TOKEN) ? true : false,
    accessToken: localStorage.getItem(ACCESS_TOKEN)
      ? localStorage.getItem(ACCESS_TOKEN)
      : '',
  },
  action
) => {
  switch (action.type) {
    case ACTION_SET_USER:
      return action.payload;
    default:
      return state;
  }
};
export default authReducer;
