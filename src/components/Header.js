import { AppBar, Avatar, Toolbar } from '@material-ui/core';
import { useSelector } from 'react-redux';
import React from 'react';
import LogoutButton from './LogoutButton';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import profilelogo from '../utils/images/profilelogo.png';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuItem: {
    marginRight: theme.spacing(2),
  },
  menuStart: {
    marginRight: theme.spacing(2),
    flex: 1,
  },
}));

function Header() {
  const auth = useSelector((state) => state.auth);
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position='static'>
        <Toolbar className='toolbar'>
          <div className={classes.menuStart}>
            <Button
              variant='contained'
              color='primary'
              size='large'
              disableElevation
              component={Link}
              to='/welcome'
            >
              Online Store
            </Button>
          </div>
          <div className={classes.menuItem}>
            {auth.isLoggedIn && (
              <LogoutButton className={classes.menuItem}></LogoutButton>
            )}
          </div>
          <div>
            {!auth.isLoggedIn && (
              <Button
                variant='contained'
                color='primary'
                component={Link}
                to='/login'
                className={classes.menuItem}
              >
                Log in
              </Button>
            )}
            {auth.isLoggedIn && (
              <Avatar
                id='profile'
                alt='profile logo'
                src={profilelogo}
                to='/profile'
                component={Link}
                className={classes.menuItem}
              />
            )}
          </div>
        </Toolbar>
      </AppBar>
    </div>
  );
}

export default Header;
