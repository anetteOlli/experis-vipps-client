import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { useDispatch } from 'react-redux';
import { setReceipt } from '../store/actions/receipt';

const ReceiptListItem = (props) => {
  const dispatch = useDispatch();

  const viewDetails = () => {
    dispatch(
      setReceipt({
        ...props.data,
      })
    );
  };

  return (
    <React.Fragment>
      <ListItem button onClick={viewDetails}>
        <ListItemText
          primary={'Receipt ID ' + props.data.receiptId}
        ></ListItemText>
      </ListItem>
    </React.Fragment>
  );
};

export default ReceiptListItem;
