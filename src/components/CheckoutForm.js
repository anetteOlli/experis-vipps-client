import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Button, Card, Paper } from '@material-ui/core';
import useMediaQuery from '@material-ui/core/useMediaQuery';

function CheckoutForm(props) {
  const [email, setEmail] = useState("");
  const [name, setName] = useState("");
  const [adress, setAdress] = useState("");

  useEffect(() => {
    if (props.theUser.familyName && props.theUser.givenName) {
      setEmail(props.theUser.email);
      setName(props.theUser.givenName + ' ' + props.theUser.familyName);
      setAdress(props.theUser.address);
    }
  }, [props.theUser]);

  function SimpleMediaQuery() {
    const matches = useMediaQuery('(max-width:380px)');
    let useStyles;
    if (matches) {
      useStyles = makeStyles((theme) => ({
        form: {
          '& > *': {
            //margin: theme.spacing(1),
            margin: '5px',
            width: '90%',
          },
        },

        lines: {
          '& > *': {
            margin: '5px',
            width: '85%',
          },
        },
        button: {
          '& > *': {
            margin: '5px',
          },
        },
      }));
    } else {
      useStyles = makeStyles((theme) => ({
        form: {
          '& > *': {
            //margin: theme.spacing(1),
            margin: '5px',
            width: '23em',
          },
        },

        lines: {
          '& > *': {
            margin: '5px',
            width: '35ch',
          },
        },
        button: {
          '& > *': {
            margin: '5px',
          },
        },
      }));
    }
    const classes = useStyles();
    return classes;
  }

  const classes = SimpleMediaQuery();

  const handleEmailChange = (event) => {
    setEmail(event.target.value);
  };
  const handleNameChange = (event) => {
    setName(event.target.value);
  };
  const handleAdressChange = (event) => {
    setAdress(event.target.value);
  };

  const saveData = (event) => {
    const formData = {
      email: email,
      name: name,
      adress: adress,
    };
    props.sendData(formData);
    event.preventDefault();
  };

  return (
    <form className={classes.form} onSubmit={saveData}>
      <Paper>
        <TextField
          className={classes.lines}
          id='email'
          label='Email'
          type='email'
          value={email}
          required
          onChange={handleEmailChange}
        />
        <br/>
        <TextField
          className={classes.lines}
          id='name'
          label='Name'
          value={name}
          required
          onChange={handleNameChange}
        />
        <br/>
        <TextField
          className={classes.lines}
          id='adress'
          label='Address'
          value={adress}
          required
          onChange={handleAdressChange}
        />
        <div className={classes.button}>
          {' '}
          <Button variant='outlined' color='primary' type='submit'>
            Save
          </Button>{' '}
        </div>
      </Paper>
    </form>
  );
}

export default CheckoutForm;
