import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import { Button, Paper } from '@material-ui/core';
import userService from '../services/userService';
import { useHistory } from 'react-router-dom';
import StatusCodes from 'http-status-codes';
import { MINLEN_PASSWORD } from '../constants/constants';

function ResetPasswordForm() {
  const history = useHistory();
  const userServ = new userService();
  const [sentEmail, setSentEmail] = useState(false);
  const [email, setEmail] = useState('');
  const [verificationCode, setVerificationCode] = useState('');
  const [password, setPassword] = useState('');
  const [passwordConfirm, setPasswordConfirm] = useState('');

  const useStyles = makeStyles((theme) => ({
    form: {
      '& > *': {
        margin: '5px',
        width: '23em',
      },
    },

    lines: {
      '& > *': {
        margin: '5px',
        width: '35ch',
      },
    },
    button: {
      '& > *': {
        margin: '5px',
      },
    },
  }));

  const classes = useStyles();

  const handleEmailChange = (event) => {
    setEmail(event.target.value);
  };

  const handleVerificationChange = (event) => {
    setVerificationCode(event.target.value);
  };

  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };

  const handlePasswordConfirmChange = (event) => {
    setPasswordConfirm(event.target.value);
  };

  //Sends a verification code to the provided email if it exists in the db
  const sendVerificationCode = async () => {
    const response = await userServ.resetVerificationCode(email);
    if (response.status === StatusCodes.INTERNAL_SERVER_ERROR) {
      alert(
        'Something went wrong in the server. Please try again. \n(Error 500)'
      );
    } else {
      setSentEmail(true);
      alert(
        'If the email is registered with us a verification code will be sent to ' +
          email
      );
    }
  };

  //Update the password
  const resetPassword = async () => {
    if (password.length < MINLEN_PASSWORD) {
      alert(
        'Password must be at least ' + MINLEN_PASSWORD + ' characters long.'
      );
      return;
    }
    if (password === passwordConfirm) {
      const response = await userServ.resetPassword(
        email,
        verificationCode,
        password
      );

      if (response.status === StatusCodes.BAD_REQUEST) {
        alert('Verification key did NOT match! (Error 400)');
      }
      if (response.status === StatusCodes.OK) {
        alert('Password has been updated.');
        history.push('/login');
      }
      if (response.status === StatusCodes.NOT_FOUND) {
        alert("Requested password wasn't even found! (Error 404)");
      }
      if (response.status === StatusCodes.INTERNAL_SERVER_ERROR) {
        alert('Something went wrong, please try again (Error 500)');
      }
    } else {
      alert('The passwords do not match.');
    }
  };

  return (
    <div>
      <Paper>
        <TextField
          className={classes.lines}
          label='Email'
          id='email'
          type='email'
          onChange={handleEmailChange}
        />
        <div className={classes.button}>
          {' '}
          <Button
            variant='outlined'
            color='primary'
            onClick={sendVerificationCode}
          >
            Send verification code
          </Button>{' '}
        </div>
      </Paper>

      {sentEmail ? (
        <form className={classes.form}>
          <Paper>
            <TextField
              className={classes.lines}
              id='verificationCode'
              label='Verification Code'
              required
              onChange={handleVerificationChange}
            />
            <TextField
              className={classes.lines}
              id='resetPassword1'
              label='password'
              type='password'
              required
              onChange={handlePasswordChange}
            />
            ¨
            <TextField
              className={classes.lines}
              id='resetPassword2'
              label='password'
              type='password'
              required
              onChange={handlePasswordConfirmChange}
            />
            <div className={classes.button}>
              {' '}
              <Button
                variant='outlined'
                color='primary'
                onClick={resetPassword}
              >
                Reset Password
              </Button>{' '}
            </div>
          </Paper>
        </form>
      ) : null}
    </div>
  );
}

export default ResetPasswordForm;
