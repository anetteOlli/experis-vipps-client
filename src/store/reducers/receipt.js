import { ACTION_SET_RECEIPT } from '../actions/receipt';

const receiptReducer = (
  state = {
    idemKey: '',
    items: [],
    receiptId: 0,
    status: '',
    time: '',
    totalPrice: 0,
  },
  action
) => {
  switch (action.type) {
    case ACTION_SET_RECEIPT:
      return action.payload;
    default:
      return state;
  }
};

export default receiptReducer;
